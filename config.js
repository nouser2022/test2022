{
  "latest_version": "1.3.8",
  "mirrors": [
    "http://gen.lib.rus.ec/",
    "http://libgen.rs/",
    "https://libgen.lc/",
    "https://libgen.is/",
    "http://libgen.pw/",
    "http://libgen.li/",
    "http://libgen.gs/"
  ],
  "searchReqPattern": "{mirror}search.php?&req={query}&page={pageNumber}&res={pageSize}&sort_mode=ASC",
  "searchByMD5Pattern": "{mirror}search.php?req={md5}&column=md5",
  "MD5ReqPattern": "{mirror}json.php?ids={id}&fields=md5"
}